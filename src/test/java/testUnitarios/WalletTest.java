package testUnitarios;

import org.junit.Before;
import org.junit.Test;
import wallet.Wallet;

import static org.junit.Assert.*;

public class WalletTest {
	Wallet wallet;

	@Before
	public void init() {
		wallet = new Wallet();
	}

	@Test
	public void testAddPositiveAmount() {
		wallet.addAmount(100);
		assertEquals(wallet.getMoney(), 100.0, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNegativeAmount() {
		wallet.addAmount(-100);
	}

	@Test
	public void withdrawPositiveAmount() {
		wallet.addAmount(500);
		wallet.withdrawAmount(100);
		assertEquals(wallet.getMoney(), 400.0, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void withdrawExceedingAmount() {
		wallet.addAmount(500);
		wallet.withdrawAmount(1000);
	}

	@Test(expected = IllegalArgumentException.class)
	public void withdrawNegativeAmount() {
		wallet.addAmount(200);
		wallet.withdrawAmount(-100);
	}
}
